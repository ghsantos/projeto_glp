/*
MAC: 2C:3A:E8:0C:A6:A6

*/
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <DHT.h>

#define DHTPIN D4
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

Adafruit_BMP085 bmp;
#define I2C_SCL D7
#define I2C_SDA D6

#define GLP_ANALOGICO A0

void readSensor() {
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    Serial.print("TemperatureDHT: ");
    Serial.println(t);

    Serial.print("umidade       : ");
    Serial.println(h);

    if (!bmp.begin()) {
        Serial.println("Could not find a valid BMP085 sensor, check wiring!");
        while (1) {}
    }

    float bp =  bmp.readPressure()/100;
    float ba =  bmp.readAltitude();
    float bt =  bmp.readTemperature();
    float dst = bmp.readSealevelPressure()/100;

    Serial.print("Pressure()/100: ");
    Serial.println(bp);

    Serial.print("Altitude      : ");
    Serial.println(ba);

    Serial.print("Temperature   : ");
    Serial.println(bt);
    
    int valor_analogico = analogRead(GLP_ANALOGICO);

    Serial.print("Pino A0       : ");
    Serial.println(valor_analogico);
    Serial.println("\n");
    
    /*
    Serial.print(h);
    Serial.print("; ");
    Serial.print(t);
    Serial.print("; ");
    Serial.print(p);
    Serial.print("; ");
    Serial.print(ba);
    Serial.print("; ");
    Serial.print(bt);
    Serial.print("; ");
    Serial.println(h);*/
}


void setup() {
  Serial.begin(115200);
  Wire.begin(I2C_SDA, I2C_SCL);
  pinMode(GLP_ANALOGICO, INPUT);
  delay(10);
}

void loop() {
  readSensor();
  delay(2000);
}


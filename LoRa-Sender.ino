/*
a       a   a           a
25, 23, 12, 13, 21, 17, 4, 22
        *   *   *   *
*/
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <DHT.h>
#include <LoRa.h>

// LoRa
#define SCK     5    // GPIO5  -- SX127x's SCK
#define MISO    19   // GPIO19 -- SX127x's MISO
#define MOSI    27   // GPIO27 -- SX127x's MOSI
#define SS      18   // GPIO18 -- SX127x's CS
#define RST     14   // GPIO14 -- SX127x's RESET
#define DI00    26   // GPIO26 -- SX127x's IRQ(Interrupt Request)

#define BAND    868E6
#define PABOOST true

// sensores
#define DHTPIN 17
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

Adafruit_BMP085 bmp;
#define I2C_SCL 21
#define I2C_SDA 13

#define GLP_ANALOGICO 12

void setup() {
  Serial.begin(115200);
  Wire.begin(I2C_SDA, I2C_SCL);

  Serial.println("LoRa Sender");

  if (!LoRa.begin(868E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }

  pinMode(GLP_ANALOGICO, INPUT);
  delay(10);
}

void loop() {
  readSensor();
  delay(2000);
}

void readSensor() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  Serial.print("TemperatureDHT: ");
  Serial.println(t);

  Serial.print("umidade       : ");
  Serial.println(h);

  if (!bmp.begin()) {
    Serial.println("Could not find a valid BMP085 sensor, check wiring!");
    while (1) {}
  }

  float bp =  bmp.readPressure()/100;
  float ba =  bmp.readAltitude();
  float bt =  bmp.readTemperature();
  float dst = bmp.readSealevelPressure()/100;

  Serial.print("Pressure()/100: ");
  Serial.println(bp);

  Serial.print("Altitude      : ");
  Serial.println(ba);

  Serial.print("Temperature   : ");
  Serial.println(bt);

  int valor_analogico = analogRead(GLP_ANALOGICO);

  Serial.print("Pino A0       : ");
  Serial.println(valor_analogico);
  Serial.println("\n");

  /*
  Serial.print(h);
  Serial.print("; ");
  Serial.print(t);
  Serial.print("; ");
  Serial.print(p);
  Serial.print("; ");
  Serial.print(ba);
  Serial.print("; ");
  Serial.print(bt);
  Serial.print("; ");
  Serial.println(h);*/

  // envia dados
  LoRa.beginPacket();
  LoRa.print("hello ");
  LoRa.print(counter);
  LoRa.endPacket();

  counter++;

  Serial.println("aqui");
}

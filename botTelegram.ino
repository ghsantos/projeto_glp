#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <FS.h>

#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <DHT.h>

#define MQ_PIN A0

#define DHTPIN D4
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

Adafruit_BMP085 bmp;
#define I2C_SCL D7
#define I2C_SDA D6

char ssid[] = "XXXXXX";
char password[] = "YYYYYY";

#define BOTtoken "XXXXXXXXX:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

#define ADMIN_USER "000000000"

WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

int Bot_mtbs = 30000; //mean time between scan messages
long Bot_lasttime;   //last time messages' scan has been done

File csvFile;
bool isMoreDataAvailable();
byte getNextByte();

String prev_chat_id = "";
String prev_command = "";

void setup() {
  Serial.begin(115200);

  Serial.print("Initializing FS....");
  openFS();
  createLogFile();

  Serial.println("done.");

  // sensors
  Wire.begin(I2C_SDA, I2C_SCL);
  pinMode(MQ_PIN, OUTPUT);

  // Set WiFi to station mode and disconnect from an AP if it was Previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  // attempt to connect to Wifi network:
  Serial.print("Connecting Wifi: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

  while(numNewMessages) {
    Serial.print("numNewMessages: ");
    Serial.println(numNewMessages);
    Serial.println("got response");
    handleNewMessages(numNewMessages);
    numNewMessages = bot.getUpdates(bot.last_message_received + 1);
  }

  if (millis() > Bot_lasttime + Bot_mtbs)  {
    Bot_lasttime = millis();
    dataLoop();

    prev_command = "";
  }

  delay(500);
}

void handleNewMessages(int numNewMessages) {
  Serial.println("handleNewMessages");
  Serial.println(String(numNewMessages));

  for (int i=0; i<numNewMessages; i++) {
    String chat_id = String(bot.messages[i].chat_id);
    String text = bot.messages[i].text;

    String from_name = bot.messages[i].from_name;
    if (from_name == "") from_name = "Guest";

    if (text == "/arquivo") {
      if (prev_chat_id == chat_id && prev_command == "/arquivo") {
        bot.sendMessage(chat_id, "Arquivo já enviando", "");
      } else {
        sendFile(chat_id);
        prev_command = "/arquivo";
      }
    } else {
      prev_command = "";

      if (text == "/delete" && chat_id == ADMIN_USER) {
        deleteFile();
      }

      if (text == "/options") {
        String keyboardJson = "[\"/arquivo\"]";
        bot.sendMessageWithReplyKeyboard(chat_id, "a", "", keyboardJson, true);
      }

      if (text == "/start") {
        String welcome = "Bem-vindo, " + from_name + ".\n";
        welcome += "/arquivo : para fazer o download do arquivo com os dados em CSV\n";
        welcome += "/options : para retornar este teclado\n";
        bot.sendMessage(chat_id, welcome, "Markdown");

        String keyboardJson = "[[\"/arquivo\", \"/teste\"],[\"/teste\"]]";
        bot.sendMessageWithReplyKeyboard(chat_id, "a", "", keyboardJson, true);

        saveUser(chat_id);
      }
    }

    prev_chat_id = chat_id;
  }
}

void dataLoop() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  int mq = analogRead(MQ_PIN);

  if (!bmp.begin()) {
    Serial.println("Could not find a valid BMP085 sensor, check wiring!");
    while (1) {}
  }

  float bp =  bmp.readPressure()/100;
  float ba =  bmp.readAltitude();
  float bt =  bmp.readTemperature();

  sendData(h, t, mq, bp, ba, bt);
  saveData(h, t, mq, bp, ba, bt);
}

void sendData(float h, float t, int mq, float bp, float ba, float bt) {
  String data = "MQ: ";
  data += mq;
  data += "\numidade: ";
  data += h;
  data += "%\ntemperatura dht: ";
  data += t;
  data += "°C\ntemperatura bmp: ";
  data += bt;
  data += "°C\npressao: ";
  data += bp;
  data += "\naltitude: ";
  data += ba;
  data += " M";

  File file = SPIFFS.open("/users", "r");

  if(file) {
    while(file.available()) {
      String chat_id = file.readStringUntil('\n');
      Serial.println(chat_id);

      bot.sendMessage(chat_id, data, "");
    }
  }
}

void saveData(float h, float t, int mq, float bp, float ba, float bt) {
  String data = "";
  data += mq;
  data += ";";
  data += h;
  data += ";";
  data += t;
  data += ";";
  data += bt;
  data += ";";
  data += bp;
  data += ";";
  data += ba;

  File file = SPIFFS.open("/log","a+");

  if(file){
    file.println(data);
    file.close();
    Serial.println("Log salvo");
  } else {
    Serial.println("Erro ao salvar log");
  }
}

bool isMoreDataAvailable(){
  return csvFile.available();
}

byte getNextByte(){
  return csvFile.read();
}

void sendFile(String chat_id) {
  csvFile = SPIFFS.open("/log", "r");

  if (csvFile) {
    Serial.print("enviando arquivo para ");
    Serial.println(chat_id);
    Serial.print("....");

    String sent = bot.sendMultipartFormDataToTelegram(
      "sendDocument", "document", "log.csv",
      "text/csv", chat_id, csvFile.size(),
      isMoreDataAvailable, getNextByte
    );

    if (sent) {
      Serial.println("arquivo enviando");
    } else {
      Serial.println("arquivo não enviando");
    }

    csvFile.close();
  } else {
    Serial.println("erro ao abrir arquivo de log");
  }
}

void deleteFile() {
  if(SPIFFS.remove("/log")){
    Serial.println("Log removido");
    bot.sendMessage(ADMIN_USER, "Log removido", "");

    createLogFile();
  } else {
    Serial.println("Erro ao remover log");
    bot.sendMessage(ADMIN_USER, "Erro ao remover log", "");
  }
}

void saveUser(String chat_id) {
  File file;

  if(SPIFFS.exists("/users")){
    Serial.println("Arquivo de users ja existe!");
    file = SPIFFS.open("/users", "a+");
  } else {
    Serial.println("Criando o arquivo de users");
    file = SPIFFS.open("/users","w+");
  }

  if(!file){
    Serial.println("Erro ao abrir arquivo de users!");
  } else {
    file.println(chat_id);
    file.close();
    Serial.println("Usuario salvo");
  }
}

void openFS(){
  if(!SPIFFS.begin()){
    Serial.println("Erro ao abrir o sistema de arquivos");
  } else {
    Serial.println("Sistema de arquivos aberto com sucesso!");
  }
}

void createLogFile(){
  File wFile;

  if(SPIFFS.exists("/log")){
    Serial.println("Arquivo de log ja existe!");
  } else {
    Serial.println("Criando o arquivo de log...");
    wFile = SPIFFS.open("/log","w+");

    if(wFile){
      wFile.println("MQ;umidade;temperatura dht;temperatura bmp;pressao;altitude");
      wFile.close();
      Serial.println("Arquivo de log criado com sucesso!");
    } else {
      Serial.println("Erro ao criar arquivo de log!");
    }
  }
}

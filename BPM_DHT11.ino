#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <DHT.h>

#define DHTPIN D4
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

Adafruit_BMP085 bmp;
#define I2C_SCL D7
#define I2C_SDA D6

float dst,bt,bp,ba;
char dstmp[20],btmp[20],bprs[20],balt[20];
bool bmp085_present=true;

void sendSensor() {
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    Serial.print("TemperatureDHT: ");
    Serial.println(t);

    Serial.print("umidade       : ");
    Serial.println(h);

    if (!bmp.begin()) {
        Serial.println("Could not find a valid BMP085 sensor, check wiring!");
        while (1) {}
    }

    float bp =  bmp.readPressure()/100;
    float ba =  bmp.readAltitude();
    float bt =  bmp.readTemperature();
    float dst = bmp.readSealevelPressure()/100;

    Serial.print("Pressure()/100: ");
    Serial.println(bp);

    Serial.print("Altitude      : ");
    Serial.println(ba);

    Serial.print("Temperature   : ");
    Serial.println(bt);
    Serial.println("\n");
}


void setup() {
  Serial.begin(115200);
  Wire.begin(I2C_SDA, I2C_SCL);
  delay(10);
}

void loop() {
  sendSensor();
  delay(2000);
}

/*
rede:  NodeMCUButano
senha: butano1010
servidor: 192.168.4.1
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include "FS.h"

#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <DHT.h>

#define MQ_PIN A0

#define DHTPIN D4
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

Adafruit_BMP085 bmp;
#define I2C_SCL D7
#define I2C_SDA D6

int pin_led_verm = D2;

/* Configuracoes de wifi */
const char *ssid = "NodeMCUButano";
const char *password = "butano1010";

float h = 0;
float t = 0;
int mq = 0;
float bp =  0;
float ba =  0;
float bt =  0;

int contUltimaLeitura = 0;

ESP8266WebServer server(80);

void openFS(){
  // Abre o sistema de arquivos
  if(!SPIFFS.begin()){
    Serial.println("Erro ao abrir o sistema de arquivos");
  } else {
    Serial.println("Sistema de arquivos aberto com sucesso!");
  }
}

void createLogFile(){
  File wFile;

  if(SPIFFS.exists("/log")){
    Serial.println("Arquivo de log ja existe!");
  } else {
    Serial.println("Criando o arquivo de log...");
    wFile = SPIFFS.open("/log","w+");

    if(wFile){
      wFile.println("MQ;umidade;temperatura dht;temperatura bmp;pressao;altitude");
      wFile.close();
      Serial.println("Arquivo de log criado com sucesso!");
    } else {
      Serial.println("Erro ao criar arquivo de log!");
    }
  }
}

// servidor /
void handleRoot() {
  String data = "<h1>Sensor de Butano</h1>";
  data = "MQ: ";
  data += mq;
  data += "<br />umidade: ";
  data += h;
  data += "%<br />temperatura dht: ";
  data += t;
  data += "*C<br />temperatura bmp: ";
  data += bt;
  data += "*C<br />pressao: ";
  data += bp;
  data += "<br />altitude: ";
  data += ba;
  data += " M";

  data += "<a href=\"/download\"><h2>Download dos dados</h2></a>";
  data += "<a href=\"/apagar\"><h2>Apagar dados</h2></a>";

  server.send(200, "text/html", data);
}

// servidor /download
void download() {
  String texto = "";

  //Faz a leitura do arquivo
  File rFile = SPIFFS.open("/log","r");
  Serial.println("Lendo arquivo...");

  char buf[1024];
  int size = rFile.size();
  while(size > 0) {
    size_t len = std::min((int)(sizeof(buf) - 1), size);
    rFile.read((uint8_t *)buf, len);
    server.client().write((const char*)buf, len);
    size -= len;
  }

  Serial.println("Finalizando leitura");
}

// servidor /apagar
void apagar() {
  String texto = "";

  if(SPIFFS.remove("/log")){
    Serial.println("Arquivo removido com sucesso!");
    texto = "Arquivo removido com sucesso!";
  } else {
    Serial.println("Erro ao remover arquivo!");
    texto = "Erro ao remover arquivo!";
  }

  server.send(200, "text/html", texto);

  createLogFile();
}

void dataLoop() {
  h = dht.readHumidity();
  t = dht.readTemperature();
  mq = analogRead(MQ_PIN);

  if (!bmp.begin()) {
    Serial.println("Could not find a valid BMP085 sensor, check wiring!");
    while (1) {}
  }

  bp =  bmp.readPressure()/100;
  ba =  bmp.readAltitude();
  bt =  bmp.readTemperature();

  saveData(h, t, mq, bp, ba, bt);

  // Verifica o nivel de gas/fumaca detectado
  if (mq > 80){
    digitalWrite(pin_led_verm, HIGH);
  } else {
    digitalWrite(pin_led_verm, LOW);
  }
}

void saveData(float h, float t, int mq, float bp, float ba, float bt) {
  String data = "";
  data += mq;
  data += ";";
  data += h;
  data += ";";
  data += t;
  data += ";";
  data += bt;
  data += ";";
  data += bp;
  data += ";";
  data += ba;

  File file = SPIFFS.open("/log","a+");

  if(file){
    file.println(data);
    file.close();
    Serial.println("Log salvo");
  } else {
    Serial.println("Erro ao salvar log");
  }
}

void setup() {
  Serial.begin(115200);

  Serial.print("Initializing FS....");
  openFS();
  createLogFile();

  Serial.println("done.");

  // sensors
  Wire.begin(I2C_SDA, I2C_SCL);
  pinMode(MQ_PIN, INPUT);

  pinMode(pin_led_verm, OUTPUT);

  Serial.println();
  Serial.print("Configuring access point...");
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", handleRoot);
  server.on("/download", download);
  server.on("/apagar", apagar);
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  server.handleClient();

  if (contUltimaLeitura >= 3000) {
    contUltimaLeitura = 0;

    dataLoop();
  }

  delay(10);
  ++contUltimaLeitura;
}

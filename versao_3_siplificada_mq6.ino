
// Definicoes dos pinos ligados ao sensor
int glp_analogico = A0;

int valor_analogico = 0;


// salvar dados em aquivo
void exibeDadosGLP() {
  // Le os dados do pino analogico do sensor de glp
  valor_analogico = analogRead(glp_analogico);

  // Mostra os dados do glp no serial monitor
  //Serial.print("; Pino A0 : ");
  Serial.println(valor_analogico);
}

void setup() {
  delay(1000);
  Serial.begin(115200);

  // Define os pinos de leitura do sensor como entrada
  pinMode(glp_analogico, INPUT);

  Serial.println("iniciado\n");
}

void loop() {
  exibeDadosGLP();

  delay(100);
}

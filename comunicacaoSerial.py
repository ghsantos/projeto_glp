# manipulacao de funcoes de tempo
# https://docs.python.org/3/library/time.html
import time

# comunicacao serial com o arduino
# https://pythonhosted.org/pyserial/
import serial

# porta USB que o arduino esta conectado (verificar na arduino IDE)
ser = serial.Serial('/dev/ttyUSB1', 115200)

# manda dados para o arduino
# ser.write('A'.encode("ascii","ignore"))

# le dados do arduino
# valor = ser.readline()

nomeArqv = 'dados.csv'

try:
    arqv = open(nomeArqv, 'w') # cada vez que executa cria um novo arquivo excluindo o anterior

    inicio = time.time() # tempo em segundos

    # a condicao 'True' faz com que execute infinitamente ou ate digitar ctrl+c
    # para executar por 50 segundos a condicao seria:
    # time.time()-inicio < 50
    while True:
        valor = ser.readline().decode().strip() # le dados do arduino e formata eles

        tempo = round(time.time()-inicio, 3) # tempo em segundos com 3 casas decimais

        print('{};{}'.format(valor, tempo)) # impime na tela

        arqv.write('{};{}\n'.format(valor, tempo)) # escreve no arquivo

except:
    arqv.close()
    print('Erro')

/*
rede:  NodeMCUButano
senha: butano1010
servidor: 192.168.4.1
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <DHT.h>
#include "FS.h"

#define DHTPIN D6
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

/* Configuracoes de wifi */
const char *ssid = "NodeMCUButano";
const char *password = "butano1010";

float umidade = 0;
float temperatura = 0;

int contUltimaLeitura = 0;

ESP8266WebServer server(80);

void openFS(){
  // Abre o sistema de arquivos
  if(!SPIFFS.begin()){
    Serial.println("Erro ao abrir o sistema de arquivos");
  } else {
    Serial.println("Sistema de arquivos aberto com sucesso!");
  }
}

void createFile(){
  File wFile;

  // Cria o arquivo se ele não existir
  if(SPIFFS.exists("/log.txt")){
    Serial.println("Arquivo ja existe!");
  } else {
    Serial.println("Criando o arquivo...");
    wFile = SPIFFS.open("/log.txt","w+");

    //Verifica a criação do arquivo
    if(!wFile){
      Serial.println("Erro ao criar arquivo!");
    } else {
      Serial.println("Arquivo criado com sucesso!");
      wFile.println("umidade;temperatura");
      wFile.close();
    }
  }
}

// servidor /
void handleRoot() {
  String texto_root = "<h1>Sensor DHT11</h1>";
  texto_root += "<br />Temperatura: ";
  texto_root += temperatura;
  texto_root += "<br />Umidade: ";
  texto_root += umidade;

  texto_root += "<a href=\"/download\"><h2>Download dos dados</h2></a>";
  texto_root += "<a href=\"/apagar\"><h2>Apagar dados</h2></a>";

  server.send(200, "text/html", texto_root);
}

// servidor /download
void download() {
  String texto = "";

  //Faz a leitura do arquivo
  File rFile = SPIFFS.open("/log.txt","r");
  Serial.println("Lendo arquivo...");

  char buf[1024];
  int size = rFile.size();
  while(size > 0) {
    size_t len = std::min((int)(sizeof(buf) - 1), size);
    rFile.read((uint8_t *)buf, len);
    server.client().write((const char*)buf, len);
    size -= len;
  }

  Serial.println("Finalizando leitura");
}

// servidor /apagar
void apagar() {
  String texto = "";

  // Remove o arquivo
  if(SPIFFS.remove("/log.txt")){
    Serial.println("Arquivo removido com sucesso!");
    texto = "Arquivo removido com sucesso!";
  } else {
    Serial.println("Erro ao remover arquivo!");
    texto = "Erro ao remover arquivo!";
  }

  server.send(200, "text/html", texto);

  createFile();
}

// salvar dados em aquivo
void salvaDados() {
  // Leitura da umidade
  umidade = dht.readHumidity();
  // Leitura da temperatura em graus Celsius
  temperatura = dht.readTemperature();

  Serial.print("temperatura: ");
  Serial.println(temperatura);
  Serial.print("umidade: ");
  Serial.println(umidade);

  String u = "";
  u = umidade;
  String t = "";
  t = temperatura;
                   // umidade;temperatura;GLP
  String dados = "" + u + ";" + t;

  //Abre o arquivo para adição (append)
  //Inclue sempre a escrita na ultima linha do arquivo
  File rFile = SPIFFS.open("/log.txt","a+");

  if(!rFile){
    Serial.println("Erro ao abrir arquivo!");
  } else {
    rFile.println(dados);

    rFile.close();
  }
}

void setup() {
  delay(1000);
  Serial.begin(115200);

  //Abre o sistema de arquivos (mount)
  openFS();
  //Cria o arquivo caso o mesmo não exista
  createFile();

  Serial.println();
  Serial.print("Configuring access point...");
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", handleRoot);
  server.on("/download", download);
  server.on("/apagar", apagar);
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  server.handleClient();

  if (contUltimaLeitura >= 100) {
    contUltimaLeitura = 0;

    salvaDados();
  }

  delay(10);
  ++contUltimaLeitura;
}

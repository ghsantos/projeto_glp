/*
rede:  NodeMCUButano
senha: butano1010
servidor: 192.168.4.1
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <DHT.h>
#include "FS.h"

#define DHTPIN D1     // what digital pin the DHT22 is conected to
#define DHTTYPE DHT22   // there are multiple kinds of DHT sensors
#define LED1 D5
#define LED2 D7

DHT dht(DHTPIN, DHTTYPE);

int pin_led_verm = D2;
int pin_led_verde = D4;

// Definicoes dos pinos ligados ao sensor
int pin_d0 = D7;
int pin_a0 = A0;

int nivel_sensor = 250;

/* Set these to your desired credentials. */
const char *ssid = "NodeMCUButano";
const char *password = "butano1010";

int valor_digital = 0;
int valor_analogico = 0;
float umidade = 0;
float temperatura = 0;

int contUltimaLeitura = 0;

ESP8266WebServer server(80);

void openFS(){
  //Abre o sistema de arquivos
  if(!SPIFFS.begin()){
    Serial.println("Erro ao abrir o sistema de arquivos");
  } else {
    Serial.println("Sistema de arquivos aberto com sucesso!");
  }
}

void createFile(){
  File wFile;

  //Cria o arquivo se ele não existir
  if(SPIFFS.exists("/log.txt")){
    Serial.println("Arquivo ja existe!");
  } else {
    Serial.println("Criando o arquivo...");
    wFile = SPIFFS.open("/log.txt","w+");

    //Verifica a criação do arquivo
    if(!wFile){
      Serial.println("Erro ao criar arquivo!");
    } else {
      Serial.println("Arquivo criado com sucesso!");
      wFile.println("umidade;temperatura;GLP");
      wFile.close();
    }
  }
}

// servidor /
void handleRoot() {
  String texto_root = "<h1>Sensor de Butano</h1>";
  texto_root += "Leitura atual: ";
  texto_root += valor_analogico;
  texto_root += "<br />Temperatura: ";
  texto_root += temperatura;
  texto_root += "<br />Umidade: ";
  texto_root += umidade;

  texto_root += "<a href=\"/download\"><h2>Download dos dados</h2></a>";
  texto_root += "<a href=\"/apagar\"><h2>Apagar dados</h2></a>";

  server.send(200, "text/html", texto_root);
}

// servidor /download
void download() {
  String texto = "";

  //Faz a leitura do arquivo
  File rFile = SPIFFS.open("/log.txt","r");
  Serial.println("Lendo arquivo...");
  Serial.println("\n\n\n\n\n\n\n\n");
  while(rFile.available()) {
    String line = rFile.readStringUntil('\n');
    
    Serial.println(line);
    
    texto += line;
    texto += "<br />";
  }
  rFile.close();
  
  Serial.println("\n\n\n\n\n\n\n\n");

  server.send(200, "text/html", texto);
}

// servidor /apagar
void apagar() {
  String texto = "";

  //Remove o arquivo
  if(SPIFFS.remove("/log.txt")){
    Serial.println("Arquivo removido com sucesso!");
    texto = "Arquivo removido com sucesso!";
  } else {
    Serial.println("Erro ao remover arquivo!");
    texto = "Erro ao remover arquivo!";
  }

  server.send(200, "text/html", texto);

  createFile();
}

// salvar dados em aquivo
void salvaDados() {
  // Le os dados do pino digital D0 do sensor
  valor_digital = digitalRead(pin_d0);
  // Le os dados do pino analogico A0 do sensor
  valor_analogico = analogRead(pin_a0);

  // Leitura da umidade
  umidade = dht.readHumidity();
  // Leitura da temperatura em graus Celsius
  temperatura = dht.readTemperature();

  // Mostra os dados no serial monitor
  Serial.print("Pino D0 : ");
  Serial.print(valor_digital);
  Serial.print(" Pino A0 : ");
  Serial.println(valor_analogico);

  String u = "";
  u = umidade;
  String t = "";
  t = temperatura;
                   // umidade;temperatura;GLP
  String dados = "" + u + ";" + t + ";" + valor_analogico;

  //Abre o arquivo para adição (append)
  //Inclue sempre a escrita na ultima linha do arquivo
  File rFile = SPIFFS.open("/log.txt","a+");

  if(!rFile){
    Serial.println("Erro ao abrir arquivo!");
  } else {
    rFile.println(dados);

    rFile.close();
  }
}

void setup() {
  delay(1000);
  Serial.begin(115200);

  // Define os pinos de leitura do sensor como entrada
  pinMode(pin_d0, INPUT);
  pinMode(pin_a0, INPUT);
  // Define pinos leds e buzzer como saida
  pinMode(pin_led_verm, OUTPUT);
  pinMode(pin_led_verde, OUTPUT);

  //Abre o sistema de arquivos (mount)
  openFS();
  //Cria o arquivo caso o mesmo não exista
  createFile();

  Serial.println();
  Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", handleRoot);
  server.on("/download", download);
  server.on("/apagar", apagar);
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  server.handleClient();

  if (contUltimaLeitura >= 6000) {
    contUltimaLeitura = 0;

    salvaDados();
  }

  // Verifica o nivel de gas/fumaca detectado
  if (valor_analogico > nivel_sensor){
    digitalWrite(pin_led_verm, HIGH);
    digitalWrite(pin_led_verde, LOW);
  } else {
    digitalWrite(pin_led_verm, LOW);
    digitalWrite(pin_led_verde, HIGH);
  }
  delay(10);
  ++contUltimaLeitura;
}
